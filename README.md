# Weather App

Ce projet a été réalisé dans le cadre d'un project d'école (Supinfo) en 2019-2020.

Le but est de créer une application web météo, de gérer les villes ajouté en favorie, de gérer les utilisateurs, le login et le logout.

Une explication plus detaillée du projet se trouve dans `sujet.pdf`.


## Pré-requis
* installer `npm`
* installer `mongo db`
* installer `reactjs`
* installer `expressjs`

## Lancer le projet
1. Ouvrez une console dans le dossier `express-server`, tapez `npm install`, puis `npm start`, le server fonctionne maintenant.
2. Ouvrez une 2ème console dans le dossier `react-client`, tapez `npm install`, puis `npm start`, le client fonctionne maintenant.

Vous avez maintenant une page web qui s'ouvre automatiquement, si ce n'est pas le cas, ouvrez-le vous même avec `localhost:3000`


## Démo 
Vous pouvez trouvez une démo du projet via ce [lien](https://drive.google.com/file/d/12pfP1TXvE7Jef3cQARfaDjjgfdVV1wyk/view?usp=sharing)
