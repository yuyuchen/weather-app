import React from 'react';
import { Redirect} from 'react-router-dom';
import AppNavBar from './NavBar';
import axios from 'axios';


import {Col, Row, Button} from 'react-bootstrap';
import Slider from 'react-slick';
import 'bootstrap/dist/css/bootstrap.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {loggedIn,convertToCelsus,getTheme, settings,apiKey, getToken,header, refreshPage, error} from './services/userServices';

import cloudy_light from "./images/cloudy_light.svg"
import raining_light from "./images/raining_light.svg"
import snowing_light from "./images/snowing_light.svg"
import storm_light from "./images/storm_light.svg"
import sunny_light from "./images/sunny_light.svg"

import cloudy_dark from "./images/cloudy_dark.svg"
import raining_dark from "./images/raining_dark.svg"
import snowing_dark from "./images/snowing_dark.svg"
import storm_dark from "./images/storm_dark.svg"
import sunny_dark from "./images/sunny_dark.svg"
    

class Home extends React.Component
{
  constructor(props){
    super(props);
    this.state={
      user:{},
      cityList:[],
      addCityPage : false,
      detailPage :false,
      detailCity: "",
      err : "",
      bodyStyle : {
        background: "linear-gradient(#9482C7, 50%,#1D0857)",
        height:"801px"     
      },
      cardStyle : {
        margin : "50px 30px",
        padding:"40px 10px",
        borderRadius: "50px",
        boxShadow:"#003EFF 0px 0px 20px -10px",
        textAlign:"center",
        cursor:"pointer",
        background: "linear-gradient(#1D0857, 70%,#CE2F7E)",
      },
      fontColor :{
        color : "black"
      }
    }
    this.signal = axios.CancelToken.source();
  }

  componentDidMount(){  
    if(loggedIn()) this.getWeather();
    if(getTheme() === "dark") 
    {
      this.setState({bodyStyle:{
        background: "linear-gradient(#9482C7, 50%,#1D0857)",
        height:"801px" 
      }})

      this.setState({cardStyle : {
        margin : "50px 30px",
        padding:"40px 10px",
        borderRadius: "50px",
        boxShadow:"#003EFF 0px 0px 20px -10px",
        textAlign:"center",
        cursor:"pointer",
        color: "white",
        background: "linear-gradient(#1D0857, 70%,#CE2F7E)",
      }})
    }else
    {
      this.setState({bodyStyle:{
        background: "none",
        height:"801px" 
      }})
      this.setState({cardStyle : {
        margin : "50px 30px",
        padding:"40px 10px",
        borderRadius: "50px",
        boxShadow:"#003EFF 0px 0px 20px -10px",
        textAlign:"center",
        cursor:"pointer"
      }})
    }
  }
  
  componentWillUnmount(){
    this.signal.cancel('Api is being canceled');
  }


  getWeather = async()=>{
    let city = [];
    let cityListWithweather = []   

    //get user info
    const header = `Authorization: Bearer ${getToken()}`;
    let getUser = await axios.get('/api/user',{headers:{header}}, {CancelToken:this.signal.token} )
      .then((response)=>{ return response.data})
      .catch(()=>{return 500})
    if(getUser !== 500)
    {
      this.setState({user:getUser});
      //get user city list
      if(getUser.cityList.length>0) city = getUser.cityList;

    }else this.setState({err:500})
    
    //get weather for each city in cityList
    if(city.length>0)
    {
      for(let c in city)
      {   
        let myCity = city[c]
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${myCity}&appid=${apiKey}`
        let getData = await axios.get(url,{CancelToken:this.signal.token})
          .then((response) => {
            let res = response.data
            let data = {
              "city": city[c],
              "descrip": res.weather[0].description,
              "temp":parseFloat(res.main['temp']),
              "min":res.main['temp_min'],
              "max":res.main['temp_max'],
              "description": res.weather[0]["description"]
            }    
            return data})
          .catch((err)=>{
          console.log(err);
          return 500
        });
        if(getData !== 500)cityListWithweather.push(getData);
        else this.setState({err:500})
      }
    } 
    this.setState({cityList:cityListWithweather}) 
  }

  handleClic = (item)=>{
    if(item === "add") this.setState({addCityPage : true})
    else {
      this.setState({detailPage:true})
      this.setState({detailCity:item})
    }
  }

  handleDelete = async (item)=>{
    let list = this.state.user.cityList;
    for(let i in list)
    {
      //delete city from the city list
      if(list[i]=== item) list.splice(i,1);  
    }   
    //send new city list to the server 
    let result = await axios.put('/api/user',{headers:{header}},{data:this.state.user}, {CancelToken:this.signal.token})
      .then((response)=>{return 200})
      .catch((err)=>{return 500}) 
    if(result === 200) refreshPage();
    else if (result === 500) this.setState({err:500});
  }

  display = ()=>{
    return(
      <div style={this.state.bodyStyle}>
        <div style={{width:"90%", margin:"auto" }}>
          <Slider {...settings}>
            {this.state.cityList.map(c=>(
              <div key={c.toString()} >
                <div style={this.state.cardStyle}>
        
                    <Col style={{right:0}}></Col>
    
                  <div onClick={()=>this.handleClic(c.city)} style={{cursor:"pointer"}}>
                    <center>     
                    <h2>{c.city}</h2>   
                      {getTheme()==="dark"?
                        <img  style={{height:"200px", width:"200px"}} src={
                          (c.descrip.includes('clear'))?sunny_dark:
                          (c.descrip.includes('clouds') || c.descrip.includes('drizzle'))?cloudy_dark:
                          (c.descrip.includes('snow'))?snowing_dark:
                          (c.descrip.includes('rain'))?raining_dark:
                          (c.descrip.includes('thunderstorm'))?storm_dark:
                          sunny_light} alt="mon" />
                        :
                        <img  style={{height:"200px", width:"200px"}} src={
                          (c.descrip.includes('clear'))?sunny_light:
                          (c.descrip.includes('clouds') || c.descrip.includes('drizzle'))?cloudy_light:
                          (c.descrip.includes('snow'))?snowing_light:
                          (c.descrip.includes('rain'))?raining_light:
                          (c.descrip.includes('thunderstorm'))?storm_light:
                          sunny_light} alt="mon" />            
                      }       
                      
                    </center> 
                    <p>{c.descrip}</p>
                    <p style={{fontWeight:"bold",fontSize:"2em"}}>{convertToCelsus(c.temp)}°C</p>
                    <Row>
                      <Col><p style={{fontSize:"1.5em"}}>{convertToCelsus(c.min)}°C <br/>Min</p></Col>
                      <Col><p style={{fontSize:"1.5em"}}>{convertToCelsus(c.max)}°C <br/>Max</p></Col>
                    </Row>  
                    
                  </div>
                  <Button variant="danger" onClick={()=>this.handleDelete(c.city) }>Delete</Button>
                                  
                  </div>
              </div>
            ))}   
            <div onClick={()=>this.handleClic("add")} >
              <div style={this.state.cardStyle}>
              <h2>Add city</h2>                               
              </div>
            </div>            
          </Slider>
        </div>                                   
      </div>
    )        
  }

  page = ()=>{
    if(loggedIn() && this.state.err !== 500)return this.display();  
    else if(this.state.err === 500)  return error() ;      
    else return <Redirect to="/auth/login" />
  }

  changePage = ()=>{
    if(this.state.detailPage){ 
      let url = '/detail/'+this.state.detailCity
      return <Redirect to={url} />
    }
    else if(this.state.addCityPage){ 
      return <Redirect to="/addCity" />
    }
    
  }

  render(){
      return(
          <div>
              <AppNavBar/>
              <div >
                  {(this.state.addCityPage || this.state.detailPage)?this.changePage(): this.page()}
              </div>          
          </div>            
      )
  }
}

export default Home;