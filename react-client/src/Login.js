
import React from 'react';
import AppNavBar from './NavBar';
import axios from 'axios';
import bcrypt from 'bcryptjs';
import {setToken,getTheme, error, loggedIn} from './services/userServices';
import {Form, Col, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { Redirect, Link } from 'react-router-dom';


class Login extends React.Component{ 
  constructor(props){
    super(props);
    this.state = {
      name:'',
      email :'',
      password: '',
      isLogin: false,
      canLogin: true,
      myError : ''  ,
      bodyStyle : {
        background: "none",
        height:"801px"     
      } 
    }
    this.signal = axios.CancelToken.source();
  }

  componentWillMount(){
    if(loggedIn()) this.setState({"canLogin":false})
    if(getTheme() === "dark") 
    {
        this.setState({bodyStyle:{
            background: "linear-gradient(#9482C7, 50%,#1D0857)",
            height:"801px" ,
            color:"white"
        }})
    }else{
        this.setState({bodyStyle:{
            background: "none",
            height:"801px" 
        }})
    }
    
  }

  componentWillUnmount(){
    this.signal.cancel('Api is being canceled');
  }

  handleChange =(event)=>{
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]:val}) 
  }

  handleSubmit = async()=>{
    let serverIsOn = await axios.get('/api/test').then(()=>{return true}).catch(()=>{return false})
    
    //If all input fields is completed
    if(this.state.email !== "" && this.state.password !== "" && serverIsOn)
    {     
        //get user from database and verify password 
       let getUser = await axios.post('/api/login',{
        CancelToken:this.signal.token,
        "email":this.state.email,
        "password":this.state.password
        }).then((response)=>{return response.data})
        .catch((err)=>{
          if (axios.isCancel(err)) console.log('Error: ', err.message); 
          else return 404
        });

      if(getUser === 404) this.setState({myError: "User not found."});
      else if (!bcrypt.compareSync(this.state.password, getUser.password)) this.setState({myError: "Invalid password."});
      else
      {
        const getToken = await axios.post('/api/login',{
          CancelToken:this.signal.token,
          "name": getUser.name,
          "email": getUser.email,
          "validPassword": true
          }).then((response)=>{return response.data})
          .catch((err)=>{
            if (axios.isCancel(err)) console.log('Error: ', err.message)
            else return 404
          });
        
        if(getToken.success)
        {
          setToken(getToken.token);
          this.setState({isLogin:true});                    
        }           
      } 

    }
    else if (!serverIsOn) this.setState({myError:500})
    else this.setState({myError: "please complete all fields"});
  }

  loginForm = () =>{
    return( 
      <Form >
        <Col sm={6}>
          <h3>Login</h3>
          <hr></hr>
          <p style={{color:"red"}}>{this.state.myError}</p>
          <Form.Group controlId="formGroupEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control onChange={this.handleChange} name="email" type="email" placeholder="Enter email" />
          </Form.Group>
          <Form.Group controlId="formGroupPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control onChange={this.handleChange} name="password" type="password" placeholder="Password" />
          </Form.Group>
          <Button onClick={this.handleSubmit} variant="success" className="btn">Valider</Button>     
          <p><br/><Link to="/auth/register">Create an account.</Link></p>   
        </Col>          
      </Form>   
    );
  }

  onLogin = ()=>{
    return <Redirect to='/' />  
  }

  render()
  {
    return(
      <div style={this.state.bodyStyle}>
        <AppNavBar></AppNavBar>
        <div className="container">
          <div className="register-form">
            {this.state.canLogin && this.state.myError !== 500?(this.state.isLogin?this.onLogin(): this.loginForm()):error()}    
          </div>
        </div>
      </div>     
    );
  }
}

export default Login;