
import React from 'react';
import AppNavBar from './NavBar';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import {loggedIn, header, error, apiKey, getTheme} from './services/userServices';
import { Col, Row, Form, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

class AddCity extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            user: {},
            finish : false,
            city: "",
            err:"",
            bodyStyle : {
                background: "none",
                height:"801px" 
                   
            }
        }
    }

    componentDidMount(){
        if(getTheme() === "dark") 
        {
            this.setState({bodyStyle:{
                background: "linear-gradient(#9482C7, 50%,#1D0857)",
                height:"801px" ,
                paddingTop:"50px" ,
                color:"white"
            }})
        }else{
            this.setState({bodyStyle:{
                background: "none",
                height:"801px" ,
                paddingTop:"50px"
            }})
        }
    }
    handleChange = (event) =>{
        this.setState({city:event.target.value});
    }

    handleSubmit = async() =>{
        if(this.state.city !== "")
        {
            //format string
            let string = this.state.city
            string = string[0].toUpperCase()
            string += this.state.city.substr(1)
    
            //Verify if the city exist
            let url = `https://api.openweathermap.org/data/2.5/weather?q=${this.state.city}&appid=${apiKey}`
            let isExist = await axios.get(url).then(() => {return true}).catch(()=>{return false})

            if(isExist)
            {
                //Get the user city list from server 
                let getUser = await axios.get('/api/user',{headers:{header}}).then((response)=>{return response.data}).catch(()=>{return 500})
                if(getUser !== 500)
                {
                    this.setState({user:getUser});
                    let list=this.state.user.cityList
                    //Add new city in list
                    list.push(string) 

                    //Put the new city List to the server
                    let result = await axios.put('/api/user',{headers:{header}},{data:this.state.user}).then(()=>{return 200}).catch(()=>{return 500}) 
                    if(result === 200) this.setState({finish:true})
                    else this.setState({err:500})
                }else this.setState({err:500})

            } else this.setState({err:"Invalid city name."})      
        } else this.setState({err:"Empty field."})
    }

    addCiityForm = ()=>{
        return(      
            <center style={this.state.bodyStyle}>
                <Form>
                    <Col sm={6}>
                        <h3>Search</h3>
                        <hr/>
                        <p style={{color:"red"}}>{this.state.err}</p>
                        <Form.Group>
                            <Form.Control onChange={this.handleChange} name="city" type="text" placeholder="Enter a city" />
                        </Form.Group>
                        <Row>
                            <Col >
                                <Button onClick={this.finish} variant="danger" className="btn">Cancel</Button>                                   
                            </Col>
                            <Col>
                                <Button onClick={this.handleSubmit} variant="success" className="btn">Valider</Button>                                    
                            </Col>
                        </Row>
                    </Col>
                </Form>
            </center>           
        )
    }


    finish = ()=>{
        this.setState({finish:true})
        return <Redirect to="/" />
    }

    render()
    {
        return(

            <div>
              <AppNavBar/>
              <div>
                  { this.state.err !== 500? 
                  (loggedIn()?(this.state.finish? this.finish(): this.addCiityForm()): <Redirect to="/" />):error()}                 
              </div>          
          </div>  
        )
    }
}

export default AddCity;