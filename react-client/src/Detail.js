import React from 'react';
import AppNavBar from './NavBar';
import axios from 'axios';

import Slider from 'react-slick';
import {Col, Row} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {loggedIn,settings,getTheme, convertToCelsus,apiKey, error} from './services/userServices';

import cloudy_light from "./images/cloudy_light.svg"
import raining_light from "./images/raining_light.svg"
import snowing_light from "./images/snowing_light.svg"
import storm_light from "./images/storm_light.svg"
import sunny_light from "./images/sunny_light.svg"
import { Redirect } from 'react-router-dom';

import cloudy_dark from "./images/cloudy_dark.svg"
import raining_dark from "./images/raining_dark.svg"
import snowing_dark from "./images/snowing_dark.svg"
import storm_dark from "./images/storm_dark.svg"
import sunny_dark from "./images/sunny_dark.svg"

class Detail extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            city :"" ,
            foreCastList :[],
            err:"",
            bodyStyle : {
                background: "none",
                height:"801px"     
            },
            cardStyle : {
                margin : "50px 30px",
                padding:"40px 10px",
                borderRadius: "50px",
                boxShadow:"#003EFF 0px 0px 20px -10px",
                textAlign:"center"
            }
        }
    }

    componentDidMount(){
        if(loggedIn())this.getForecast();
        if(getTheme() === "dark") 
        {
            this.setState({bodyStyle:{
                background: "linear-gradient(#9482C7, 50%,#1D0857)",
                height:"801px" 
            }});
            this.setState({cardStyle : {
                margin : "50px 30px",
                padding:"40px 10px",
                borderRadius: "50px",
                boxShadow:"#003EFF 0px 0px 20px -10px",
                textAlign:"center",
                color: "white",
                background: "linear-gradient(#1D0857, 70%,#CE2F7E)",
            }});
            }else
            {
            this.setState({bodyStyle:{
                background: "none",
                height:"801px" 
            }});
            this.setState({cardStyle : {
                margin : "50px 30px",
                padding:"40px 10px",
                borderRadius: "50px",
                boxShadow:"#003EFF 0px 0px 20px -10px",
                textAlign:"center"
            }});
        }
    }

    getForecast = async()=>{
        let myCity = this.props.match.params.city;
        this.setState({city:myCity})
        let url = `https://api.openweathermap.org/data/2.5/forecast?q=${myCity}&appid=${apiKey}`;
     
        let serverIsOn = await axios.get('/api/test').then(()=>{return true}).catch(()=>{return false})
        
        //Get forecast
        let getForeCast = await axios.get(url)
        .then((response) => {
            let res = response.data
            let foreCast = []      
            for(let i=0; i <40; i += 8)
            {
                let day = {
                    date: new Date(res.list[i].dt_txt.toString().slice(0,10)).toDateString().slice(0,10),
                    min:res.list[i].main['temp_min'],
                    max:res.list[i].main['temp_max'],
                    description: res.list[i].weather[0]["description"]
                }           
                foreCast.push(day)           
            }    
            return foreCast   
        })
        .catch((err)=>{
            console.log(err)
            return 500         
        });
        if(getForeCast !== 500 && serverIsOn) this.setState({foreCastList:getForeCast})
        else this.setState({err:500})
    }

    display = ()=>{
        return(
            <div style={this.state.bodyStyle}>
                <div style={{width:"90%", margin:"auto"}}>
                    <center>                              
                        <h1 style={{paddingTop:"50px"}}>
                            <a href="/" style={{textDecoration:"none", color:"black"}}>&#x3008; &nbsp;&nbsp;</a>
                            {this.state.city}
                            </h1>
                    </center>         
                    <Slider {...settings}>
                        {this.state.foreCastList.map(c=>(
                            <div key={c.toString()} >
                                <div style={this.state.cardStyle}>           
                                    <center>   
                                        <h2>{c.date}</h2>  
                                        {getTheme()==="dark"?
                                            <img  style={{height:"200px", width:"200px"}} src={
                                            (c.description.includes('clear'))?sunny_dark:
                                            (c.description.includes('clouds') || c.description.includes('drizzle'))?cloudy_dark:
                                            (c.description.includes('snow'))?snowing_dark:
                                            (c.description.includes('rain'))?raining_dark:
                                            (c.description.includes('thunderstorm'))?storm_dark:
                                            sunny_light} alt="mon" />  
                                        :
                                            <img  style={{height:"200px", width:"200px"}} src={
                                            (c.description.includes('clear'))?sunny_light:
                                            (c.description.includes('clouds') || c.description.includes('drizzle'))?cloudy_light:
                                            (c.description.includes('snow'))?snowing_light:
                                            (c.description.includes('rain'))?raining_light:
                                            (c.description.includes('thunderstorm'))?storm_light:
                                            sunny_light} alt="mon" />                                         
                                        }
                                                                     
                                    </center> 
                                    <p>{c.description}</p>
                                    <Row>
                                        <Col><p style={{fontSize:"1.5em"}}>{convertToCelsus(c.min)}°C <br/>Min</p></Col>
                                        <Col><p style={{fontSize:"1.5em"}}>{convertToCelsus(c.max)}°C <br/>Max</p></Col>
                                    </Row>                                   
                                </div>
                            </div>
                        ))}                      
                    </Slider>
                </div>                                   
            </div>
        )        
    }

    render()
    {
        return(
            <div>
              <AppNavBar/>
              <div>
                  {(this.state.foreCastList && this.state.err !==500) ?(loggedIn()? this.display():<Redirect to="/"/>):error()}
              </div>          
          </div>  
        )
    }
}

export default Detail;