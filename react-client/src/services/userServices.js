
import React from 'react';
import decode from 'jwt-decode';

export const logout = ()=> {
    localStorage.removeItem("token");   
}

export const loggedIn = ()=>{  
    if(getToken() != null)
        return true;
    else
        return false; 
}

export const convertToCelsus=(myString)=>{
    try{
        let temp = parseInt(myString)-273
        return temp
    }catch{
        return "error"
    }

}

export const decodeToken = ()=>{
    let token = getToken();
    let data = decode(token);
    return {"name": data["name"], "email": data["email"]}
}


export const getToken= ()=>{
    return localStorage.getItem("token");
}
export const setToken =(token)=>{
    localStorage.setItem("token", token);
}
export const removeToken = () =>{
    localStorage.removeItem("token");
}

export const getTheme = ()=>{
  return localStorage.getItem("theme");
}

export const refreshPage = () =>{
    window.location.reload();
}

export const header = `Authorization: Bearer ${getToken()}`;
export const apiKey = `9eb22dfd5c35bd7d54551f66c3cbbda7`;

export const settings = {
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow:4,
    slidesToScroll: 4,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1224,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: false,
          dots: true
        }
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };

export const error = () =>{
    return(
        <center>
            <h3 style={{color:"red"}} >Error</h3>
            <p style={{color:"red"}}>An error occurred while processing your request</p>
            <p style={{color:"red"}}>Please verify if your server is running</p>
        </center>
    )
}