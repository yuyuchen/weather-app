
import React from 'react';
import { BrowserRouter, Route} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';

import Register from './Register';
import Login from './Login';
import Home from './Home';
import AddCity from './AddCity';
import Detail from './Detail';

class App extends React.Component {
  render()  {
    return(
      <div>       
         <BrowserRouter>      
            <Route path="/" component={AppBody} />
          </BrowserRouter>       
      </div>         
    );
  }
}

class AppBody extends React.Component{
  render()
  {
    return(          
  
        <BrowserRouter>    
          <Route exact path="/" component={Home} />   
          <Route exact path="/auth/login" component={Login} />
          <Route exact path="/auth/register" component={Register} />   
          <Route exact path="/addCity" component={AddCity} />  
          <Route exact path="/detail/:city" component={Detail} />  
        </BrowserRouter>   

    );
  }
}


export default App;


/*
  let decoded = {};
  decoded =decode(token);
  console.log("token = " + token );
  console.log(decoded["email"]);
  
*/
