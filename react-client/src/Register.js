
import React from 'react';
import AppNavBar from './NavBar';
import axios from 'axios';
import bcrypt from 'bcryptjs';
import {error, loggedIn, getTheme} from './services/userServices';
import {Form, Col, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

class Register extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      name : '',
      email :'',
      password: '',
      registerError: '',
      isRegisted : false,
      canRegisted : true, 
      err: "",
      bodyStyle : {
        background: "none",
        height:"801px"     
      } 
    }
    this.signal = axios.CancelToken.source();
  }

  componentWillMount(){
    if(loggedIn()) this.setState({"canRegisted":false})
    if(getTheme() === "dark") 
    {
      this.setState({bodyStyle:{
          background: "linear-gradient(#9482C7, 50%,#1D0857)",
          height:"801px" ,
          color:"white"
      }})
    }else{
      this.setState({bodyStyle:{
          background: "none",
          height:"801px" 
      }})
    }
  }

  componentWillUnmount(){
    this.signal.cancel('Api is being canceled');
  }
  
  handleChange =(event)=>{
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]:val})
  }

  handleSubmitForm = async()=>{

    let serverIsOn = await axios.get('/api/test').then(()=>{return true}).catch(()=>{return false})

    //Rules for user input
    var nameReg = new RegExp (/^(?=.*[a-z])/);
    var emailReg = new RegExp(/^([\w-.]+)@((?:[\w]+.)+)([a-zA-Z]{2,4})/i);
    var pwdReg = new RegExp(/(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/);

    var nameIsValid = nameReg.test(this.state.name) && this.state.name.length > 2;
    var emailIsValid = emailReg.test(this.state.email);
    var pwdIsValid = pwdReg.test(this.state.password) && this.state.password.length >= 8 && this.state.password.length < 20;

    //if all fields is completed.
    if(this.state.name !== "" && this.state.email !=="" && this.state.password !== "" && serverIsOn)
    {
      //if all fields are valid
      if(nameIsValid && emailIsValid && pwdIsValid)
      {
        const sendNewUser = await axios.post('/api/register',{
          CancelToken:this.signal.token,
          "name" : this.state.name,
          "email": this.state.email,
          "password": bcrypt.hashSync(this.state.password)
        })
        .then(()=>{
          return 201
        }).catch((err)=>{
          if (axios.isCancel(err)) {
            console.log('Error: ', err.message); 
            return 401
          } else return 401
        });

        if(sendNewUser === 401) this.setState({err:'Email is already exist.'});
        else this.setState({isRegisted: true});      
      }
      else
      {
        //check error
        if(!nameIsValid) this.setState({err:'Your name must contain at least 3 characters, and begin with an alpha character.'}); 
        else if (!emailIsValid)this.setState({err:'Invalid email.'});
        else if (!pwdIsValid)this.setState({err:'Your password must be beetwen 8 - 20 characters, include the following character types : uppercase letter, lowercase letter, digit number.'}); 
        else this.setState({err:'err'});
      }
    }
    else if (!serverIsOn) this.setState({err: 500});
    else this.setState({err: "please complete all fields"});

  }

  registerCompleted = () =>{
    return(
      <div>
        <h3>Resgiter completed</h3>
        <a href='/auth/login'>Please to login.</a>
      </div>
    );
  }

  userForm = () =>{
    return (
      <Form>
        <Col sm={6}>
          <h3>Register</h3>
          <hr></hr>
          <p style={{color:"red"}}>{this.state.err}</p><br/>
          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control name="name" type="text" placeholder="Enter your name" onChange={this.handleChange} />
          </Form.Group>
          <Form.Group >
            <Form.Label>Email address</Form.Label>
            <Form.Control name="email" type="email" placeholder="Enter email" onChange={this.handleChange}/>
          </Form.Group>
          <Form.Group >
            <Form.Label>Password</Form.Label>
            <Form.Control name="password" type="password" placeholder="Password" onChange={this.handleChange}/>
          </Form.Group>
          <Button onClick={this.handleSubmitForm} variant="success" className="btn">Valider</Button>
        </Col>        
      </Form>
    );
  }

  render(){
    return(     
      <div style={this.state.bodyStyle}>
        <AppNavBar></AppNavBar>
        <div className="container">
          <div className="register-form">
            {this.state.canRegisted && this.state.err !== 500?(this.state.isRegisted? this.registerCompleted() : this.userForm()):error()}      
          </div>
        </div>
      </div>    
    );
  }
}

export default Register;