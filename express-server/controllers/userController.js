import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import decode from 'jwt-decode';
const user = mongoose.model('users');
const secret = 'mysecret';

export const register = async (req,res) =>{
    let checkUser = await user.findOne({email : req.body.email})
    if(checkUser == null)
    {
      let newUser = {
        "name": req.body.name,
        "email":req.body.email,
        "password":req.body.password,
        "cityList": ["Paris"]
      }
      await user.create(newUser);
      return res.status(201).send('Successfully registed !');
    }
    else
      return res.status(400).send('User already exist');
}

export const login = async (req,res) =>{
    let getUser = await user.findOne({email : req.body.email})
    if(getUser != null)
    {
      if(req.body.validPassword)
      {
        const payload = {"name":req.body.name, "email":req.body.email};
        const token = jwt.sign(payload,secret, {expiresIn:'1h'})      
        return res.status(200).json({
          "success" : true,
          "token":token
        })       
      }else
      {
        return res.status(200).json(getUser);
      }         
    }
    else
      return res.status(404).send('User not found.');   
}


export const withAuth = async(req,res, next)=>{
  let h = req.headers.header;
  if(h.split(' ')[1]=== "Bearer" ) next();
  else return res.status(401).send("Access denied")
}


export const getUser = async(req, res)=>{
  let h = req.headers.header;
  let decoded = decode(h.split(' ')[2]);
  let userInfo = await user.findOne({"email" : decoded.email});
  return res.json(userInfo)
}

export const updateUser = async(req,res)=>{
  let h = req.body.headers.header
  let decoded = decode(h.split(' ')[2])
  if(h.split(' ')[1]=== "Bearer" ){
    await user.findByIdAndUpdate(req.body._id,req.body)
    .then(()=>{
      return res.sendStatus(200)
    }).catch((err)=>{
      return res.sendStatus(500)
    })
  }else
  {
    return res.status(401).send("Access denied");
  } 
}



