
import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';

const app = express();
const PORT = 5000;

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://localhost:27017/meteoDB`, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify : false
})

app.use(bodyParser.json());


require('./models/user');
require('./routes/userRoutes')(app);


if (process.env.NODE_ENV === 'production') {
    app.use(express.static('client/build')); 
    const path = require('path');
    app.get('*', (req,res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))
    }) 
  }

app.listen(PORT, ()=>{
    console.log('Your application is running...')
})

